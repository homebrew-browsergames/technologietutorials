<?php
/* Copyright (C) 2011-2020  Stephan Kreutzer
 *
 * This file is part of Tutorial "Image-Maps mit einfacher PHP-Unterstützung".
 *
 * Tutorial "Image-Maps mit einfacher PHP-Unterstützung" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 or
 * any later version, as published by the Free Software Foundation.
 *
 * Tutorial "Image-Maps mit einfacher PHP-Unterstützung" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with Tutorial "Image-Maps mit einfacher PHP-Unterstützung".  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/database.inc.php
 * @author Stephan Kreutzer
 * @since 2011-08-19
 */



$mysql_connection = @mysqli_connect("localhost", "tutuser", "password");

if ($mysql_connection != false)
{
    if (@mysqli_query($mysql_connection, "USE tutorial") == false)
    {
        @mysqli_close($mysql_connection);
        $mysql_connection = false;
    }
}
